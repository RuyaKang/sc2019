import numpy as np
L = np.sort(np.random.randint(1, 20, 8))
print("L = ", L)
n = len(L)

x = 12 #target

for i,l in enumerate(L):

    if x == l:

        ind = i
        break

def searchb(L, x, debug = False):
    """ Use binary search to find target, x, in sorted array
    """

    #check if L is sorted

    assert type(x) is int or type(x) is float, "error, x must be numeric"

    ind = -100
         
    istart = 0
    iend = n-1

    while istart <= iend:
           
        #median element
        imid = (iend + istart) // 2 #symbol "//" returns integer

        if debug:

            print("istart, imid, iend = ", istart, imid, iend)
        
        #compare to median, discard half of array as appropriate
        if x == L[imid]:

            ind = imid
            break

        elif x < L[imid]:

            iend = imid - 1
    
        else:

            istart = imid + 1

    return ind

######################################################################################################

M = np.random.randint(1, 20, 8)
S = M.copy()

for i in range(1, n):

    for j in range(i-1, -1, -1):

        print("i, j = ", i, j)

        if S[j+1] <- S[j]:

            S[j], S[j+1] = S[j+1], S[j]

        else:

            break

####################################################################################################

def merge(L, R):

    nL = len(L)
    nR = len(R)

    indL = 0
    indR = 0

    M = []

    for i in range(nL+nR):

        if L[indL] < R[indR]:

            M.append(L[indL])
            indL = indL + 1

            if indL == nL:

                M.extend(R[indR:])
                break

        else:

            M.append(R[indR])
            indR = indR + 1

            if indR == nR:

                M.extend(L[indL:])
                break

    return M

