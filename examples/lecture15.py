import numpy as np

tf = 5
Nt = 100

t = np.linspace(0, tf, Nt+1)
t = t[:-1] # no need for the last term because of periodicity

T = 5
f = 4/T # want four periods in the time domain
A = np.sin(2*np.pi*f*t)

c = np.fft.fft(A)

n = np.arange(-Nt/2, Nt/2)

semilogy(n, np.fft.fftshift(abs(c))/Nt, 'x') # make a plot with log scaling on the y-axis

#############################################################################################
from scipy.signal import hann

tf = 5
Nt = 100

t = np.linspace(0, tf, Nt+1)
t = t[:-1] # no need for the last term because of periodicity

T = 5
f = 4/T # want four periods in the time domain
A = np.sin(2*np.pi*f*t)

H = hann(Nt)
B = A*H

d = np.fft.fft(B)

n = np.arange(-Nt/2, Nt/2)

semilogy(n, np.fft.fftshift(abs(d))/Nt, 'x') # make a plot with log scaling on the y-axis
