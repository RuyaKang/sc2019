import networkx as nx

Grandom = nx.gnp_random_graph(1000, 0.05)
# nx.draw(Grandom, node_shape = '.')

cl = nx.clustering(Grandom, 500)
path = nx.shortest_path(Grandom, source=0, target=500)


######################################################################
# BFS
G = nx.Graph()
edges = [[1,2], [1,3], [2,4], [3,4], [3,5], [5,6]]
G.add_edges_from(edges)
s = 1
Q = [s]

nodes = G.nodes()
z = [0 for i in nodes]
d = [-1000 for i in nodes]
L = [nodes, z, d]
L[1][s-1] = 1
L[2][s-1] = 0

# while len(Q) > 0:
#     n = Q.pop(0)

#     for v in G.adj[n].keys():

#         if L[1][v-1] == 0:
#             # node searched
#             L[1][v-1] = 1

#             # distance from the first node
#             L[2][v-1] = L[2][n-1]+1
            
#             # node list
#             Q.append(v)

#     print('n = %d, Q = ' %(n), Q)

######################################################################
# DFS
# while len(Q) > 0:
#     n = Q.pop()

#     for v in G.adj[n].keys():

#         if L[1][v-1] == 0:
#             # node searched
#             L[1][v-1] = 1

#             # distance from the first node
#             L[2][v-1] = L[2][n-1]+1
            
#             # node list
#             Q.append(v)

#     print('n = %d, Q = ' %(n), Q)