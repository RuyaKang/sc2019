import networkx as nx

e = [[1,2,3], [1,5,2], [2,5,1], [2,3,1], [3,4,2], [4,5,5], [4,6,1]]
G = nx.Graph()
G.add_weighted_edges_from(e)

# unexplored nodes
Udict = {}
Udict[5] = 0

# expored nodes
Edict = {}

# find node in Udict with smallest distance
while len(Udict) > 0:
    dmin = 2000000
    for k,v in Udict.items():
        if v < dmin:
            dmin = v
            n = k

    # remove smallest distance node and update provisional
    # distances of its unexplored neighbours
    for m, en, wn in G.edges(n, data = 'weight'):
        ddist = dmin + wn
        if en in Edict:
            pass
        elif en in Udict:
            Udict[en] = min(Udict[en], ddist)
        else:
            Udict[en] = ddist
    
    Edict[n] = Udict.pop(n)