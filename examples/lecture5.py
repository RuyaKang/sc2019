S = "ATGTTGTACCGTATCGG" # Gene sequence
P = "GTA" #Pattern

nS = len(S)
nP = len(P)

ind = []

# Iterate through elements of S
for i in range (nS - nP + 1):

    match = True

    # Compare (up tp) 3 elements of S with elements of P
    for j in range(nP):

        if P[j] != S[i+j]:

            match = False
            break

    if match: ind.append(i) # If there's a match for all 3 elemtnts, store location