import numpy as np
U, S, VT = np.linalg.svd(A)
A1 = np.outer(U[:, 0], VT[0, :])*S[0]
imshow(A1, cmap='gray')

Nterms = 5
AN = np.outer(U[:, :Nterms], np.dot(np.diag(S[:Nterms]), VT[:Nterms, :]))
imshow(AN, cmap='gray')