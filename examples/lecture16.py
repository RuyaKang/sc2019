import numpy as np

# generate grid
L = 5
N = 100

x = np.linspace(0, L, N+1)
x = x[:-1]
h = x[1]-x[0]
hfac = 1/(2*h)

# generate function
f = np.sin(2*np.pi*4/L*x)

# compute derivative
df = np.zeros_like(f)
df[1:N-1] = hfac*f[2:N] - f[0:N-2]

df[0] = hfac*(f[1]-f[-1])
df[-1] = hfac*(f[0]-f[-2])

# compute exact derivative
df_exact = 2*np.pi*4/L*np.cos(2*np.pi*4/L*x)

error = np.abs(df-df_exact)
error_total = error.sum()/N